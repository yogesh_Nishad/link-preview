# Link Preview

#### Description
To show preview of a link, we need to know four things: the link itself, image URL, title and description of the link, the module is here to fulfill the need.

#### Examples
const { linkPreview } = require(\`link-preview-node\`);

linkPreview(\`npmjs.com\`)<br />
    &nbsp;&nbsp;&nbsp;&nbsp;.then(resp => {<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;console.log(resp);<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_/\* *{ image: 'https://static.npmjs.com/338e4905a2684ca96e08c7780fc68412.png',<br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;title: 'npm | build amazing things',<br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;description: '',<br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;link: 'http://npmjs.com' }* \*/_<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Note that **''** is used when value of any detail of the link is not available<br />
    &nbsp;&nbsp;&nbsp;&nbsp;}).catch(catchErr => {<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;console.log(catchErr);<br />
    &nbsp;&nbsp;&nbsp;&nbsp;});<br />

// In case you are comfortable with callbacks<br />
const { linkPreviewCallback } = require(\`link-preview-node\`);<br />

linkPreviewCallback(\`npmjs.com\`, (err, resp) => {<br />
    &nbsp;&nbsp;&nbsp;&nbsp;console.log(err ? err : resp);<br />
});<br />

// An error is returned in first argument of callback and catch block of promise when any invalid URL is supplied to the functions<br />
linkPreview(\`fdsafsgd.com\`)<br />
    &nbsp;&nbsp;&nbsp;&nbsp;.then(ans => {<br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;console.log(ans);<br />
    &nbsp;&nbsp;&nbsp;&nbsp;}).catch(catchErr => {<br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;console.log(catchErr);<br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_/\* *{ Error: getaddrinfo ENOTFOUND fdsafsgd.com fdsafsgd.com:80<br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;at GetAddrInfoReqWrap.onlookup [as oncomplete] (dns.js:56:26)<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;errno: 'ENOTFOUND',<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;code: 'ENOTFOUND',<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;syscall: 'getaddrinfo',<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hostname: 'fdsafsgd.com',<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;host: 'fdsafsgd.com',<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;port: 80 }* \*/_<br />
    &nbsp;&nbsp;&nbsp;&nbsp;});

#### Reference
I would like to thank Rahul Taneja (https://irtaneja.com/) for helping me in this module. He can be contacted at connect@irtaneja.com.

#### Report An Issue
If you find any issue in the module, you can report it at https://gitlab.com/nmb94/link-preview/issues.