`use strict`;
const axios = require(`axios`);
const cheerio = require(`cheerio`);

module.exports = {
  linkPreview: (link) => {
    return new Promise((resolve, reject) => {
      getLinkPreview(link)
        .then((response) => resolve(response))
        .catch((catchErr) => reject(catchErr));
    });
  },
  linkPreviewCallback: (link, callback) => {
    getLinkPreview(link)
      .then((response) => callback(null, response))
      .catch((catchErr) => callback(catchErr, null));
  },
};

const getLinkPreview = (link) => {
  return new Promise((resolve, reject) => {
    if (typeof link != `string` || !link.trim()) {
      return reject(new Error(`Your link must be a valid string.`));
    }
    !link.startsWith(`http://`) && !link.startsWith(`https://`)
      ? (link = `http://${link}`)
      : ``;
    link.startsWith(`https`)
      ? httpsLinkPreview(link, resolve, reject)
      : httpLinkPreview(link, resolve, reject);
  });
};

const httpsLinkPreview = (link, resolve, reject) => {
  try {
    axios
      .get(link)
      .then((html) => {
        let $ = cheerio.load(html.data);
        let titleTag = $(`title`).text();
        let titleMeta = $(`meta[property="og:title"]`).attr(`content`);
        let title = titleMeta ? titleMeta : titleTag;
        const img = $(`meta[property="og:image"]`).attr(`content`);
        const descriptionMeta = $(`meta[property="og:description"]`).attr(
          `content`
        );
        const descriptionTag = $(`meta[property="description"]`).text();
        const description = descriptionMeta ? descriptionMeta : descriptionTag;
        resolve({
          image: img || ``,
          title: title || ``,
          description: description || ``,
          link: link,
        });
      })
      .catch((error) => {
        if (
          error.message.startsWith(
            `Hostname/IP does not match certificate's altnames:`
          )
        ) {
          link = link.replace(`https`, `http`);
          axios
            .get(link)
            .then((html2) => {
              let $ = cheerio.load(html2.data);
              let titleTag = $(`title`).text();
              let titleMeta = $(`meta[property="og:title"]`).attr(`content`);
              let title = titleMeta ? titleMeta : titleTag;
              const img = $(`meta[property="og:image"]`).attr(`content`);
              const descriptionMeta = $(`meta[property="og:description"]`).attr(
                `content`
              );
              const descriptionTag = $(`meta[property="description"]`).text();
              const description = descriptionMeta
                ? descriptionMeta
                : descriptionTag;
              resolve({
                image: img || ``,
                title: title || ``,
                description: description || ``,
                link: link,
              });
            })
            .catch((error2) => reject(error2));
        } else {
          return reject(error);
        }
      });
  } catch (catchErr) {
    reject(catchErr);
  }
};

const httpLinkPreview = (link, resolve, reject) => {
  try {
    axios
      .get(link)
      .then((html) => {
        let $ = cheerio.load(html.data);
        let titleTag = $(`title`).text();
        let titleMeta = $(`meta[property="og:title"]`).attr(`content`);
        let title = titleMeta ? titleMeta : titleTag;
        if (
          title == `Application Control Violation` ||
          title == `Web Filter Violation`
        ) {
          link = link.replace(`http`, `https`);
          axios
            .get(link)
            .then((html2) => {
              $ = cheerio.load(html2.data);
              titleTag = $(`title`).text();
              titleMeta = $(`meta[property="og:title"]`).attr(`content`);
              title = titleMeta ? titleMeta : titleTag;
              const img = $(`meta[property="og:image"]`).attr(`content`);
              const descriptionMeta = $(`meta[property="og:description"]`).attr(
                `content`
              );
              const descriptionTag = $(`meta[property="description"]`).text();
              const description = descriptionMeta
                ? descriptionMeta
                : descriptionTag;
              resolve({
                image: img || ``,
                title: title || ``,
                description: description || ``,
                link: link,
              });
            })
            .catch((error2) => reject(error2));
        } else {
          const img = $(`meta[property="og:image"]`).attr(`content`);
          const descriptionMeta = $(`meta[property="og:description"]`).attr(
            `content`
          );
          const descriptionTag = $(`meta[property="description"]`).text();
          const description = descriptionMeta
            ? descriptionMeta
            : descriptionTag;
          resolve({
            image: img || ``,
            title: title || ``,
            description: description || ``,
            link: link,
          });
        }
      })
      .catch((error) => reject(error));
  } catch (catchErr) {
    reject(catchErr);
  }
};
